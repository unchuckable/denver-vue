export enum ConnectionState {
  CONNECTING,
  CONNECTED,
  DISCONNECTED,
}

export type Message = {
  raw: string;
  messageClass: string;
  text?: string;
  timestamp: string;
  origin?: string;
  picture?: string;
};
