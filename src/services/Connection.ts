let connection: WebSocket;

let messageBuffer = '';
let intervalId: NodeJS.Timeout;

interface Connection {
  connect: () => void;
  disconnect: () => void;
  sendMessage: (message: string) => void;
  sendP2PMessage: (message: string) => void;

  onMessage?: (message: string) => void;
  onP2PMessage?: (message: string) => void;
  onError?: (error: Event) => void;

  onConnect?: () => void;
  onDisconnect?: () => void;
}

export default {
  connect() {
    connection = new WebSocket(
      'wss://javanatic.nullpointer.net/denver-web/denvue'
    );

    connection.onerror = (message: Event) => this.onError?.(message);

    connection.onmessage = (event) => {
      const message: string = event.data;
      const index = message.indexOf(':');
      if (index) {
        const type = message.substring(0, index);
        const data = message.substring(index + 1);

        const dialogAnnotation = String.fromCodePoint(0xfffd);

        if (type === 'stc') {
          messageBuffer += data;
          if (messageBuffer.endsWith(dialogAnnotation)) {
            messageBuffer = messageBuffer.substring(
              0,
              messageBuffer.length - 2
            );
            this.onMessage?.(messageBuffer);
            messageBuffer = '';
          } else if (messageBuffer.endsWith('\n')) {
            this.onMessage?.(messageBuffer);
            messageBuffer = '';
          }
        } else if (type === 'p2p') {
          this.onP2PMessage?.(data);
        }
      }
    };

    connection.onclose = () => {
      clearInterval(intervalId);
      this.onDisconnect?.();
    };

    intervalId = setInterval(() => {
      this.sendMessage('');
    }, 30000);

    this.onConnect?.();
  },

  disconnect() {
    connection.close();
  },

  isConnected() {
    return connection == null;
  },

  sendMessage(message: string) {
    connection.send('cts:' + message + '\n');
  },

  sendP2PMessage(message: string) {
    connection.send('p2p:' + message);
  },
} as Connection;

export function sendMessage(message: string) {
  connection.send('cts:' + message + '\n');
}
