import { EventBus } from 'quasar';
import { ConnectionState } from 'src/interfaces/Types';

const eventBus = new EventBus<{
  connected: () => void;
  disconnected: () => void;
  newState: (state: ConnectionState) => void;
  trigger: (trigger: string) => void;
  'denvue-message': (message: string) => void;
}>();

export function useEventBus() {
  return eventBus;
}
