import { Message } from 'src/interfaces/Types';
import { ConnectionState, useMushStateStore } from 'src/stores/MushStateStore';
import { sendMessage } from './Connection.js';
import { useEventBus } from './EventBus.js';
import { AnsiUp } from './ansi_up.js';
import stripAnsi from 'strip-ansi';

const convert = new AnsiUp();
convert.use_classes = true;

const eventBus = useEventBus();
const stateStore = useMushStateStore();

type Processor = {
  pattern: RegExp | RegExp[];
  replacement?: string;
  triggerName?: string;
  suppress?: boolean;
  action?: (
    message: Message,
    match: RegExpExecArray
  ) => Message[] | Message | void;
};

const newProcessors: { [name: string]: Processor } = {
  image: {
    pattern: [
      /IC Picture: \u001b\[32m\u001b\[1m(.*?)\u001b\[0m(.*)/,
      /^IC Picture: (http[^\s]*)$/,
    ],
    action: (message, match) => {
      message.picture = match[1];
      message.raw = match[2] || '';
      return [message];
    },
  },
  room: {
    pattern: [
      /Obvious exits /,
      / has arrived\.$/,
      / has left\.$/,
      / has connected\.$/,
      / has disconnected\.$/,
    ],
    suppress: false,
    action: () => sendMessage('+denvue/scan'),
  },
  loginScreen: {
    pattern: /lets you log on and look around as a Guest/,
    action: () => stateStore.setState(ConnectionState.CONNECTING),
  },
  loggedIn: {
    pattern: /Shadowrun is a Registered Trademark of FASA Corporation/,
    action: () => stateStore.setState(ConnectionState.CONNECTED),
  },
  denvue: {
    pattern: /^DENVUE:([^:]*):(.*)/,
    action: (message, match) => {
      processDenvueCommand(match[1], match[2]);
    },
    suppress: true,
  },
  fugueEdit: {
    pattern: /FugueEdit > (.*)/,
    suppress: true,
    action: (message, match) => {
      stateStore.setInputLine(match[1]);
    },
  },
  nospoof: {
    pattern: /^\[.*\((#[0-9]+)\)(?:{(.*)})?] (.*)/s,
    action: (message, match) => {
      message.origin = match[1];
      message.raw = match[3];
    },
  },
};

const messageClasses: { [key: string]: RegExp[] } = {
  dialog: [/> $/],
  room: [/Obvious exits /],
  pages: [
    /^You paged .* with/,
    /.* pages:/,
    /^Long distance to/,
    /^From afar, /,
  ],
  ooc: [/^<<OOC>>/],
  info: [/---\[ Shadowrun Denver \]---/],
  notifications: [/^<<(Watchfor|Game|Nom|Rumor)/, /GAME:/],
  channels: [/^\[([^#\]]*)] /],
};

const noSpoofPattern = /^\[.*\((#[0-9]+)\)(?:{(.*)})?] /g;

function findMessageClass(message: string): string {
  let unformattedMessage = stripAnsi(message);
  unformattedMessage = unformattedMessage.replaceAll(noSpoofPattern, '');

  for (const thisMessageClass in messageClasses) {
    for (const thisPattern of messageClasses[thisMessageClass]) {
      if (thisPattern.test(unformattedMessage)) {
        return thisMessageClass;
      }
    }
  }
  return 'standard';
}

export function processDenvueMessages(message: string) {
  const lines = message.split('\r\n');
  for (const line of lines) {
    processDenvueMessage(line.substring(7));
  }
}

export function processDenvueMessage(message: string) {
  const index = message.indexOf(':');
  if (index) {
    const command = message.substr(0, index);
    const args = message.substr(index + 1);
    processDenvueCommand(command, args);
  }
}

export function processDenvueCommand(command: string, args: string) {
  switch (command) {
    case 'typing': {
      stateStore.updatePlayerTyping(args, new Date().getTime() / 1000);
      break;
    }
    case 'players': {
      try {
        stateStore.setPlayersByJson(args);
      } catch (e) {
        console.log('Error updating players', args, e);
      }
      break;
    }
    case 'who-am-i': {
      const parts = args.split('|');
      stateStore.setUserId(parts[0]);
      stateStore.setUserName(parts[1]);
      stateStore.userPic = parts[2];
      stateStore.cloudKey = parts[3];
      break;
    }
    case 'who': {
      try {
        stateStore.addPlayerNamesFromJson(args);
      } catch (e) {
        console.log('Error updating player names', args, e);
      }
    }
  }
}

function removeLastCr(message: string): string {
  if (message.endsWith('\r\n')) {
    return message.substring(0, message.length - 2);
  }
  return message;
}

function splitMessage(message: Message): Message[] {
  return removeLastCr(message.raw)
    .split('\r\n')
    .map((line) => ({
      raw: line,
      timestamp: message.timestamp,
      messageClass: message.messageClass,
    }));
}

function getMatch(message: Message, pattern: RegExp | RegExp[]) {
  if (Array.isArray(pattern)) {
    for (const thisPattern of pattern) {
      const match = thisPattern.exec(message.raw);
      if (match) {
        return match;
      }
    }
    return undefined;
  } else {
    return pattern.exec(message.raw);
  }
}

function runProcessor(processor: Processor, message: Message): Message[] {
  const match = getMatch(message, processor.pattern);
  if (!match) {
    return [message];
  }

  if (processor.triggerName) {
    eventBus.emit('trigger', processor.triggerName);
  }

  const resulting = processor.action?.(message, match);

  if (resulting) {
    if (Array.isArray(resulting)) {
      return resulting;
    } else {
      return [resulting];
    }
  }

  if (processor.suppress) {
    return [];
  }

  return [message];
}

// const linkPattern = /(https?:\/\/([^\s\'\"<]+))/g;
const linkPattern = /((https?:\/\/[^\s\'\"<]+))/g;

export default {
  processMessage(rawMessage: string): Message[] {
    const message: Message | undefined = {
      raw: rawMessage,
      timestamp: new Date().toLocaleTimeString(),
      messageClass: findMessageClass(rawMessage),
    };

    let messages = splitMessage(message);

    for (const thisProcessorId in newProcessors) {
      const thisProcessor = newProcessors[thisProcessorId];
      const messagesAfter = messages.flatMap((message) =>
        runProcessor(thisProcessor, message)
      );
      messages = messagesAfter;
    }

    convert.reset();

    let lastActivePlayer: string | undefined = undefined;

    for (const thisMessage of messages) {
      if (thisMessage.origin) {
        if (thisMessage.messageClass !== 'notifications') {
          lastActivePlayer = thisMessage.origin;
        }
      }
      thisMessage.text = convert.ansi_to_html(thisMessage.raw);
      thisMessage.text = thisMessage.text.replaceAll(
        linkPattern,
        '<a referrerpolicy="no-referrer" target="_blank" href="$1">$1</a>'
      );
    }

    if (lastActivePlayer) {
      stateStore.lastActivePlayer = lastActivePlayer;
    }

    return messages;
  },
};
