import { reactive, ref } from 'vue';
import { defineStore } from 'pinia';

import { Message } from 'src/interfaces/Types';

import stripAnsi from 'strip-ansi';

import { customAlphabet } from 'nanoid';

const nanoid = customAlphabet('1234567890abcdef', 6);

export class Filters {
  pages = true;
  ooc = true;
  info = true;
  notifications = true;
  channels = true;

  constructor(init?: Partial<Filters>) {
    Object.assign(this, init);
  }
}

export class Log {
  id = nanoid();
  name = '';
  lines: string[] = [];
  recording = false;
  version = 0;
  cloudVersion = 0;
  filter: Filters = new Filters();

  public constructor(init?: Partial<Log>) {
    Object.assign(this, init);
  }
}

function isMessageValidForLog(message: Message, log: Log) {
  if (!log.recording) {
    return false;
  }

  switch (message.messageClass) {
    case 'pages':
      return log.filter.pages;
    case 'ooc':
      return log.filter.ooc;
    case 'info':
    case 'dialog':
      return log.filter.info;
    case 'notifications':
      return log.filter.notifications;
    case 'channels':
      return log.filter.channels;
  }
  return true;
}

export const useLogStore = defineStore('log', {
  state: () => ({
    logs: reactive<Log[]>([]),
    activeLog: ref<Log | null>(null),
  }),
  actions: {
    addToLogs(message: Message) {
      for (const thisLog of this.logs) {
        if (isMessageValidForLog(message, thisLog)) {
          thisLog.lines.push(stripAnsi(message.raw));
          thisLog.version++;
        }
      }
    },
    createLog(name: string) {
      const thisLog = new Log({ name: name });
      this.addLog(thisLog);
      return thisLog;
    },
    copyLog(log: Log) {
      const copy = {
        name: 'Copy of ' + log.name,
        id: nanoid(),
        lines: log.lines,
        recording: log.recording,
        version: log.version,
        cloudVersion: -1,
        filter: log.filter,
      };
      this.addLog(copy);
      return copy;
    },
    addLog(log: Log) {
      if (typeof log.filter.channels === 'undefined') {
        log.filter.channels = true;
      }

      if (typeof log.filter.notifications === 'undefined') {
        log.filter.notifications = true;
      }

      this.logs.push(log);
      this.logs.sort((a, b) => (a.name > b.name ? 1 : -1));
    },
    deleteLog(id: string) {
      this.logs = this.logs.filter((log) => log.id !== id);
    },
    clearLogs() {
      this.logs = [];
    },
  },
});
