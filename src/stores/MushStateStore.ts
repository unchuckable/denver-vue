import { defineStore } from 'pinia';
import { ref, reactive } from 'vue';

export enum ConnectionState {
  CONNECTING,
  CONNECTED,
  DISCONNECTED,
}

export type Player = {
  id: string;
  name: string;
  shortDesc?: string;
  picture?: string;
  typingTime?: number;
};

const inputLine = ref<string>('');
const state = ref<ConnectionState>(ConnectionState.DISCONNECTED);
const userId = ref<string | undefined>(undefined);
const userName = ref<string | undefined>(undefined);
const userPic = ref<string | undefined>(undefined);
const cloudKey = ref<string | undefined>(undefined);

const lastActivePlayer = ref<string | undefined>(undefined);
const activePlayer = ref<string | undefined>(undefined);

const playerNameMap = reactive<Map<string, string>>(new Map());

const players = ref<Player[]>([]);

export const useMushStateStore = defineStore('mushState', {
  state: () => ({
    inputLine,
    state,
    userId,
    userName,
    userPic,
    cloudKey,
    players,
    activePlayer,
    lastActivePlayer,
    playerNameMap,
  }),
  actions: {
    setState(newStatus: ConnectionState) {
      state.value = newStatus;
      userId.value = undefined;
      userName.value = undefined;
      userPic.value = undefined;
      players.value = [];
      cloudKey.value = undefined;
    },
    setUserId(newUserId: string) {
      userId.value = newUserId;
    },
    setUserName(newUserName: string) {
      userName.value = newUserName;
    },
    setPlayers(newPlayers: Player[]) {
      players.value = newPlayers;
    },
    setPlayersByJson(message: string) {
      const newPlayers: Array<Player> = JSON.parse(message);
      newPlayers.sort((a, b) => a.name.localeCompare(b.name));
      newPlayers.filter((player) => player.id !== userId.value);
      players.value = newPlayers;
    },
    updatePlayerTyping(playerId: string, timestamp: number) {
      const player = players.value.find((player) => player.id === playerId);
      if (player) {
        player.typingTime = timestamp;
      }
    },
    setInputLine(newInputLine: string) {
      inputLine.value = newInputLine;
    },
    addPlayerNamesFromJson(message: string) {
      const newPlayerNames: Map<string, string> = new Map(
        Object.entries(JSON.parse(message))
      );
      console.log(newPlayerNames);
      newPlayerNames.forEach((name: string, id: string) =>
        playerNameMap.set(id, name)
      );
    },
  },
});
