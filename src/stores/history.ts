import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useHistoryStore = defineStore('history', () => {
  const commands = ref<string[]>([]);

  function addToHistory(message: string) {
    if (!message || message.length == 0) {
      return;
    }
    commands.value = commands.value.filter((item) => {
      return item !== message;
    });
    commands.value.unshift(message);
    if (commands.value.length > 100) {
      commands.value.pop();
    }
  }
  return { commands, addToHistory };
});
